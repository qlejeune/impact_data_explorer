import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json



sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

try:
		iso = sys.argv[1]
except:
		iso = 'DEU'

print(iso)
COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/'+iso)

os.system('mkdir '+COU._working_dir+'slices/')

if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/'+iso+'/masks/*')) == 0:
	print('create mask first')

	os.system('cp -r '+'/p/tmp/pepflei/regioClim_2p0/'+iso+'/'+iso+'_adm_shp '+COU._working_dir+'/'+iso+'_adm_shp')

	COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm0.shp')
	COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm0.shp', long_name='NAME_ISO', short_name='ISO')
	COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm1.shp')
	COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm1.shp', long_name='NAME_1', short_name='HASC_1')
	COU.load_shapefile()

	COU.create_masks_overlap(input_file='/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/historical/HadGEM2-ES/tas_day_HadGEM2-ES_historical_r1i1p1_EWEMBI_18610101-18701231.nc4')
	COU.create_masks_latweighted()

COU.load_mask()
masks = COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['latWeight']

impModels = np.unique([ff.split('/')[-1] for ff in glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*')])
gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
scenarios = ['rcp26','rcp45','rcp60','rcp85']
wlvls = ['1.5','2.0','2.5','3.0','3.5','4.0']

# get reference
scenario = 'historical'
wlvl = '1986-2006'
tmp_mod = {}
for gcm in gcms:
	for impModel in impModels:
		filename = '/p/tmp/pepflei/runoff/'+'/'.join([gcm,scenario,impModel])+'/'+'_'.join([gcm,scenario,impModel,wlvl])+'.nc'
		if os.path.isfile(filename):
			tmp = xr.open_dataset(filename)['qtot'].loc[masks.lat,masks.lon]
			tmp_mod[impModel+'_'+gcm] = tmp
ref = xr.concat(tmp_mod.values(), dim='modelCombi')
ref = ref.assign_coords(modelCombi=list(tmp_mod.keys()))

class MyEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, np.integer):
			return int(obj)
		elif isinstance(obj, np.floating):
			return float(obj)
		elif isinstance(obj, np.ndarray):
			return obj.tolist()
		else:
			return super(MyEncoder, self).default(obj)

areaAverage = {reg:{scen:{wlvl:{} for wlvl in wlvls} for scen in scenarios} for reg in masks.region.values}

for scenario in scenarios:
	for wlvl in wlvls:
		tmp_mod = {}
		for gcm in gcms:
			for impModel in impModels:
				filename = '/p/tmp/pepflei/runoff/'+'/'.join([gcm,scenario,impModel])+'/'+'_'.join([gcm,scenario,impModel,wlvl])+'.nc'
				if os.path.isfile(filename):
					tmp = xr.open_dataset(filename)['qtot'].loc[masks.lat,masks.lon]
					tmp_mod[impModel+'_'+gcm] = tmp

		if len(list(tmp_mod.keys())) > 0:
			proj = xr.concat(tmp_mod.values(), dim='modelCombi')
			proj = proj.assign_coords(modelCombi=list(tmp_mod.keys()))

			usable_models = proj.modelCombi[np.isin(proj.modelCombi, ref.modelCombi)]
			proj = proj.loc[usable_models]

			proj_median = proj.median('modelCombi')

			# is this the way to go?
			median_diff = proj_median - ref.loc[usable_models].median('modelCombi')
			median_diff_rel = ( proj_median - ref.loc[usable_models].median('modelCombi') ) / ref.loc[usable_models].median('modelCombi')

			agree = median_diff.copy()*0
			for mod in proj.modelCombi.values:
				agree += np.sign(median_diff) == np.sign(proj.loc[mod] - ref.loc[mod])

			agree /= np.float(proj.shape[0]) / 100

			out = {'projection':[],'projection_vs_reference':[],'projection_vs_reference_relative':[],'model_agreement':[],'lat':[],'lon':[]}
			ys,xs = np.where(np.isfinite(masks.loc[iso]))
			for y,x in zip(masks.lat[ys].values,masks.lon[xs].values):
				out['lat'].append(y)
				out['lon'].append(x)
				out['projection'].append(np.float(proj_median.loc[y,x]))
				out['projection_vs_reference'].append(np.float(median_diff.loc[y,x]))
				out['projection_vs_reference_relative'].append(np.float(median_diff_rel.loc[y,x]))
				out['model_agreement'].append(np.float(agree.loc[y,x]))

			out['used_models'] = list(usable_models.values)
			out['scneario'] = scenario
			out['wlvl'] = wlvl

			with open(COU._working_dir+'slices/'+'_'.join([scenario,wlvl])+'.json', 'w') as fp:
				json.dump(out, fp, sort_keys=True, indent=4, cls=MyEncoder)


			for reg in masks.region.values:
				mask = masks.loc[reg]
				areaAverage[reg][scenario][wlvl] = np.float(np.sum(proj_median * mask))

for reg in masks.region.values:
	with open(COU._working_dir+'areaAverage/'+reg+'.json', 'w') as fp:
		json.dump(areaAverage[reg]['rcp85'], fp, sort_keys=True, indent=4, cls=MyEncoder)












#
