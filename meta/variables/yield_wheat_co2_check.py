import glob, operator
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields (wheat)',
	 'orig_name': 'yield-whe',
	 'short_name': 'yield_wheat_co2',
	 'mask_file' : '/p/tmp/pepflei/impact_data_explorer_data/agriculture_masks/physical_area_wheat_720x360.nc',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield_wheat_co2/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/historical/*historical*_co2_yield-whe*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/future/*rcp*_co2_yield-whe*',
	 'irrealistic_values' : [{'operator' : operator.gt, 'threshold' : 50.}, {'operator' : operator.lt, 'threshold' : -50.}],	 
	 }