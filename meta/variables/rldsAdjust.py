import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Surface Downwelling Longwave Radiation',
	 'orig_name': 'rldsAdjust',
	 'short_name': 'rldsAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': 'np.mean',
	 'monthly_aggreagation': 'np.mean',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/rldsAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/rldsAdjust_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/rldsAdjust_*'}