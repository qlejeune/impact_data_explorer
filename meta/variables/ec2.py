import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Annual Expected Damages - River Flood',
	 'orig_name': 'ec2',
	 'short_name': 'ec2',
	 'source': 'CLIMADA',
	 'rel-abs': nan,
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan}