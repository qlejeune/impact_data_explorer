import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Temperature of Lake Water',
	 'orig_name': 'watertemp',
	 'short_name': 'watertemp',
	 'source': 'ISIMIP',
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': 'np.mean',
	 'monthly_aggreagation': 'np.mean',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/watertemp/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/lakes_local/*/*/historical/*historical*_watertemp_*daily*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/lakes_local/*/*/*/*_watertemp_*'}