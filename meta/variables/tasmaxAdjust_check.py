import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Daily Maximum Near-Surface Air Temperature',
	 'orig_name': 'tasmax',
	 'short_name': 'tasmaxAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.mean,
	 'monthly_aggreagation': np.mean,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/tasmaxAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tasmax_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tasmax_*'}