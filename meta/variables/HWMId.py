import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Heat Wave Magnitude Index daily',
	 'orig_name': 'HWMId',
	 'short_name': 'HWMId',
	 'source': 'https://gitlab.com/climateanalytics/ngfs/-/tree/ISIMIP/Heatwave',
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': '???',
	 'monthly_aggreagation': '???'}