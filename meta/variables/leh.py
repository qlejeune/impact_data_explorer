import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Land area exposed to heatwave',
	 'orig_name': 'leh',
	 'short_name': 'leh',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': nan,
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'correlation with HWMId',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/leh/*/*/*/*'}