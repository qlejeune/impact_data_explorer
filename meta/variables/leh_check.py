import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Land area exposed to heatwave',
	 'orig_name': 'leh',
	 'short_name': 'leh',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'absolute',
	'correction_factor' : 100.0,
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'correlation with HWMId',
    'excludeImpModels' : ['MPI-HM'],
    'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/leh/*/*/*/*',
    'in_files': 
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*leh*') + \
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*leh*')
    }