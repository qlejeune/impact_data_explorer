import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Population exposed to wildfire',
	 'orig_name': 'pew',
	 'short_name': 'pew',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'nan',
    'excludeImpModels' : ['MPI-HM'],
    'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/pew/*/*/*/*',
    'in_files': 
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*pew*') + \
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*pew*')
    }