import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields  (wheat)',
	 'orig_name': 'yield',
	 'short_name': 'yield',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'per growing season',
	 'seasonal_aggreagation': 'sum',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_yield_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_yield_*'}