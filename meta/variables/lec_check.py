import glob
import numpy as np
details = {'overwrite': 'no',
	'long_name': 'Land area exposed to crop failure',
	'orig_name': 'lec',
	'short_name': 'lec',
	'source': 'ISIMIP - Secondary Output',
	'rel-abs': 'absolute',
	'correction_factor' : 100.0,
	'temporal_resolution': 'yearly',
	'seasonal_aggreagation': 'nan',
	'excludeImpModels' : ['MPI-HM','VISIT'],
	'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/lec/*/*/*/*',
	'in_files': 
		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*lec*') + \
		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*lec*')
	}
