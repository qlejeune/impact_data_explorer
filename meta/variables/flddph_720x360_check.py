import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Flood Depth',
	 'orig_name': 'flddph_720x360',
	 'short_name': 'flddph_720x360',
	 'source': 'ISIMIP - Extreme Events',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/flddph_720x360/*/*/*/*',
	 'in_files' : glob.glob('/p/projects/isimip/isimip/inga/ngfs/flood_data/*/flddph_720x360*')
	 }