import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Annual Expected Damages - River Flood',
	 'orig_name': 'Band1',
	 'short_name': 'ec2',
	 'source': 'CLIMADA',
     'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/ec2/*/*/*/*',
	 'seasonal_aggreagation': 'nan'}
