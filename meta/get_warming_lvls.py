import os,sys,glob,time,collections,gc,pickle
import numpy as np
from netCDF4 import Dataset,num2date
import matplotlib.pylab as plt
import pandas as pd

sys.path.append('/Users/peterpfleiderer/Projects/git-packages/wlcalculator/app/')
import wacalc.CmipData as CmipData; reload(CmipData)


models = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']

warming_levels = pd.DataFrame()
warming_levels['wlvl'] = np.arange(1,5.55,0.1)

for model in models:
	cmipdata = CmipData.CmipData('CMIP5',[model.lower()],['rcp26','rcp45','rcp60','rcp85','historical'])
	cmipdata.get_cmip(runs = ['r1i1p1'])
	cmipdata.compute_period( [1986,2006], [1850,1900], warming_levels['wlvl'], window=21)
	lvls=cmipdata.exceedance_tm
	for scenario in lvls.scenario:
		warming_levels[model+'_'+scenario] = lvls[scenario,:].values

warming_levels.to_csv('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/warming_lvls_cmip5_21_years.csv')



# for the compare function
years = [2030,2050,2100]
warming_levels = pd.DataFrame()
warming_levels['scenario'] = ['rcp26','rcp45','rcp60','rcp85']

models = np.unique([fl.split('/')[-1].split('.')[0] for fl in glob.glob('/Users/peterpfleiderer/Projects/git-packages/wlcalculator/data/cmip5_ver002/*')])

cmipdata = CmipData.CmipData('CMIP5',models,['rcp26','rcp45','rcp60','rcp85','historical'])
# cmipdata.get_cmip(runs = ['r1i1p1'])

for year in years:
	cmipdata.get_cmip()
	cmipdata.compute_warming([1850,1900], [1986,2006], [year-10,year+10])
	lvls = cmipdata.warming_attr
	warming_levels[year] = lvls.values.round(1)

warming_levels.to_csv('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/warming_levels_for_compare_function.csv')

wlvls = np.unique(list(warming_levels.iloc[:,1:].values.flatten()) + [1.5,2.0,2.5,3.0])
count=0
for i in wlvls:
	for j in [w for w in wlvls if w<i]:
		print(i,j)
		count+=1




#
