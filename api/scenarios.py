import flask
from flask import request, jsonify

import os,sys, importlib, glob,gc,time
import numpy as np
import xarray as xr
import pandas as pd

import json

possible_paths = [
    '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
    '/home/tooli/impact_data_explorer/data/',
    '/Users/niklasschwind/Documents/impact_data_explorer/data/'
]

for data_path in possible_paths:
    if os.path.isdir(data_path):
        break

ngfs_data = pd.read_table('../meta/scenarios/snapshot_world.csv', sep=',')

scenarios = {}
scenarios_meta = [
    {'id' : 'rcp26', 'name' : 'RCP26', 'basescenario' : False, 'primary':True},
    {'id' : 'rcp45', 'name' : 'RCP45', 'basescenario' : False, 'primary':True},
    {'id' : 'rcp60', 'name' : 'RCP60', 'basescenario' : False, 'primary':True},
    {'id' : 'rcp85', 'name' : 'RCP85', 'basescenario' : False, 'primary':True},
    {'id' : 'h_cpol', 'name' : 'NGFS current policies', 'basescenario' : False, 'primary':True},
    {'id' : 'o_1p5c', 'name' : 'NGFS net-zero 2050', 'basescenario' : False, 'primary':True},
    {'id' : 'd_delfrag', 'name' : 'NGFS delayed 2°C', 'basescenario' : False, 'primary':True},
    {'id' : 'cat_current', 'name' : 'CAT current policies', 'basescenario' : True, 'primary':True},
]


###########
# rcp ....
###########
# data = pd.read_table('../meta/scenarios/warming_levels_for_compare_function.csv', sep=',', header=0)
# for rcp in data.scenario:
#     scenarios[rcp] = pd.DataFrame()
#     scenarios[rcp]['year'] = [2030,2050,2070]
#     scenarios[rcp]['wlvl median'] = [str(w) for w in data.loc[data.scenario == rcp].iloc[0,2:].values]

for scenario in ['rcp26','rcp45','rcp60','rcp85']:
    tmp = pd.read_table('../meta/scenarios/nauels17gmd_MAGICC_GMT_'+scenario.upper()+'.csv', sep=';', header=0)
    scenarios[scenario] = pd.DataFrame()
    scenarios[scenario]['year'] = list(range(2000,2101))
    scenarios[scenario]['median'] = tmp.loc[tmp['Percentile / Year'] == '50th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p5'] = tmp.loc[tmp['Percentile / Year'] == '5th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p95'] = tmp.loc[tmp['Percentile / Year'] == '95th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['wlvl median'] = [str(np.round(w,1)) for w in scenarios[scenario]['median']]
    scenarios[scenario]['wlvl p5'] = [str(np.round(w,1)) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['wlvl p95'] = [str(np.round(w,1)) for w in scenarios[scenario]['p95']]



###########
# NGFS ....
###########
IAM = 'REMIND-MAgPIE 2.1-4.2'
IAM = 'MESSAGEix-GLOBIOM 1.0'
# IAM = 'GCAM5.3_NGFS'

for scenario in ['h_cpol','o_1p5c','d_delfrag']:
    med = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|MED')]
    p5 = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|P5')]
    p95 = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|P95')]

    scenarios[scenario] = pd.DataFrame()
    scenarios[scenario]['year'] = list(range(2000,2101))
    scenarios[scenario]['median'] = med.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p5'] = p5.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p95'] = p95.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['wlvl median'] = [str(np.round(w,1)) for w in scenarios[scenario]['median']]
    scenarios[scenario]['wlvl p5'] = [str(np.round(w,1)) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['wlvl p95'] = [str(np.round(w,1)) for w in scenarios[scenario]['p95']]

# scenarios['h_cpol'].loc[np.isin(scenarios['h_cpol'].year,range(2020,2050))]

########
# CAT
########


#runs_high = pd.read_table('../meta/scenarios/MAGICCresults_TRENDHI_CATFINAL_200831/CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL.OUT', header=17, delim_whitespace=True)
#runs_low = pd.read_table('../meta/scenarios/MAGICCresults_TRENDLO_CATFINAL_200831/CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL.OUT', header=17, delim_whitespace=True)

runs_high = pd.read_table('../meta/scenarios/MAGICCresults_TRENDHI_CATFINAL_200831/CPP_HIGH_CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL_UPDATED.OUT', header=17, delim_whitespace=True)
runs_low = pd.read_table('../meta/scenarios/MAGICCresults_TRENDLO_CATFINAL_200831/CPP_LOW_CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL_UPDATED.OUT', header=17, delim_whitespace=True)

runs = pd.concat([runs_high,runs_low.iloc[:,1:]], axis=1, ignore_index=True)

runs.columns = ['YEARS'] + list(range(1200))

med = np.median(runs.iloc[:,1:].values, axis=1) + 0.61
p5 = np.percentile(runs.iloc[:,1:].values, 5, axis=1) + 0.61
p95 = np.percentile(runs.iloc[:,1:].values, 95, axis=1) + 0.61
years = runs.YEARS.values

scenarios['cat_current'] = pd.DataFrame()
scenarios['cat_current']['year'] = list(range(2000,2101))
scenarios['cat_current']['median'] = med[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['p5'] = p5[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['p95'] = p95[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['wlvl median'] = [str(np.round(w,1)) for w in scenarios['cat_current']['median']]
scenarios['cat_current']['wlvl p5'] = [str(np.round(w,1)) for w in scenarios['cat_current']['p5']]
scenarios['cat_current']['wlvl p95'] = [str(np.round(w,1)) for w in scenarios['cat_current']['p95']]

