import os,sys, importlib, glob,gc,time, gzip
import flask
from flask import request, jsonify, make_response,Response
import numpy as np
import xarray as xr
import pandas as pd
import json,io

# this is a nice way of getting the error message when something goes wrong in a "try: " block
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# here I test whether the api is run on the server or on my computer and adjust the paths accordingly
possible_paths = [
    '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
    '/home/tooli/impact_data_explorer/data/',
    '/Users/niklasschwind/Documents/impact_data_explorer/data/'
]
for data_path in possible_paths:
    if os.path.isdir(data_path):
        break

# should be changed to something else than "DEBUG" at some point?
app = flask.Flask(__name__)
app.config["DEBUG"] = True

# import scenario information, the meta information, and dummy dicts that are given out if something went wrong
from scenarios import *
from prepare_all_meta import *
from dummies import *

# add the information about scenarios into the meta dict
meta['scenarios'] = scenarios_meta

def compress(content):
    content = gzip.compress(json.dumps(content).encode('utf8'), 5)
    response = make_response(content)
    response.headers['Content-length'] = len(content)
    response.headers['Content-Encoding'] = 'gzip'
    response.mimetype = 'application/json'
    return response


@app.route('/api/', methods=['GET'])
def home():
    # the home site just displays a few example links
    domain_name = 'https://cie-api.climateanalytics.org'

    home_message = '''<h1>Impact Data Explorer API</h1>
    get some data:<br>
    * Maps: <a href="XXXX/api/map-difference/?iso=BRA&var=prAdjust&season=JJA&format=csv&scenarios=h_cpol&wlvls=3.0"> XXXX/api/map-difference/?iso=BRA&var=prAdjust&season=JJA&format=csv&scenarios=h_cpol&wlvls=3.0 </a><br>
    * time series: <a href="XXXX/api/ts/?iso=BRA&region=BRA&scenario=cat_current&var=prAdjust&season=JJA&aggregation_spatial=area&format=csv">XXXX/api/ts/?iso=BRA&region=BRA&scenario=cat_current&var=prAdjust&season=JJA&aggregation_spatial=area&format=csv</a><br>
    * topojson: <a href="XXXX/api/shapes/?iso=BRA">XXXX/api/shapes/?iso=BRA</a><br>
    * meta: <a href="XXXX/api/meta/">XXXX/api/meta/</a><br>
    '''
    return home_message.replace('XXXX',domain_name)

@app.route('/api/meta/', methods=['GET'])
def api_meta():
    # this gives out a dict with all meta information
    # this dict "meta" is prepared in the script prepare_all_meta.py 
    return jsonify(meta)

@app.route('/api/shapes/', methods=['GET'])
def api_shapes():
    print('_'*20)
    print('shapes')
    print(request.args)
    try:
        iso = request.args.get('iso', default='DEU', type=str)
    except:
        error_message = '''
        some arguments are missing.
        please provide:
        ?iso=???
        '''
        return error_message

    '''
    iso = 'RUS'
    '''
    print(iso)

    out = {}
    # load the stored topojson
    with open(data_path+'/topojsons/'+iso+'_0_topo.json') as f:
        out['country'] = json.load(f)

    # print(out['country']['objects']['data']['geometries'][0])
    if 'properties' not in  out['country']['objects']['data']['geometries'][0].keys():
         out['country']['objects']['data']['geometries'][0]['properties'] = {}
    out['country']['objects']['data']['geometries'][0]['properties']['initial_bounds'] = meta['cou_bounds'][iso]

    # add the names of all subregions
    if os.path.isfile('../data/masks/'+iso+'/region_names.pkl'):
        tmp = pickle.load(open('../data/masks/'+iso+'/region_names.pkl', 'rb'))
        out['country']['objects']['data']['geometries'][0]['properties']['subregions'] = {k:v for k,v in tmp.items() if k is not None}

    # json_string = json.dumps(out,ensure_ascii = False)
    # response = Response(json_string,content_type="application/json; charset=utf-8" )
    # return response
    # print(out)
    return compress(out)
    # return jsonify(out)


@app.route('/api/map-difference/', methods=['GET'])
def api_map_difference():
    print('_'*20)
    print('map_difference')
    print(request.args)
    try:
        iso = request.args.get('iso', default='DEU', type=str)
        var = request.args.get('var', default='prAdjust', type=str)
        season = request.args.get('season', default='annual', type=str)
        mode = request.args.get('format', default='json', type=str)
        wlvls = request.args.get('wlvls', default='', type=str).split(',')
        years = request.args.get('years', default='', type=str).split(',')
        scens = request.args.get('scenarios', default='', type=str).split(',')
        cheat = request.args.get('cheat', default='no', type=str)
    except:
        error_message = '''
        some arguments are missing.
        please provide:
        ?iso=???&var=???&season=???&format=csv|json
        '''
        return error_message

    '''
    iso = 'FRA'
    var = 'fldfrc'
    season = 'annual'
    mode = 'json'
    wlvls = ['1.5','1.1']
    years = [2050]
    scens = ['rcp85','rcp26']
    '''

    print(wlvls,years,scens)
    # I use try - except here to avoid the app to crash
    # if something goes wrong, a dummy response is given out and the error is written in a log file
    try:
        # don|t show predefined countries
        if var in meta['exclude_countries'].keys():
            assert iso not in meta['exclude_countries'][var], 'we excluded this country for this variable'

        # don|t show predefined countries
        if var in meta['exclude_vars_for_maps'] and cheat != 'yes':
            assert var not in meta['exclude_vars_for_maps'], 'we excluded this variable'

        # load details about the variable
        # these are similar to the information in the google sheet
        details = [d for d in meta['vars'] if d['id'] == var][0]

        if years != ['']:
            years = [int(y) for y in years]

        # hack to set a different reference warming level for variables that don't have the 1986-2006 reference period
        # if (details['source'] == 'climada' or var in ['flddph_720x360','fldfrc_720x360']) and wlvl_low == '1986-2006':
            # wlvl_low = '1.1'

        # transform scenario and zear information into wlvls
        # different options
        if wlvls == ['']:
            if len(years) == 1 and len(scens) == 2:
                s1 = scenarios[scens[0]]
                w1 = s1.loc[s1.year == years[0],'wlvl median'].values[0]
                s2 = scenarios[scens[1]]
                w2 = s2.loc[s2.year == years[0],'wlvl median'].values[0]
                wlvls = [w1,w2]
            elif len(years) == 2 and len(scens) == 1:
                scenario_ = scenarios[scens[0]]
                wlvls = scenario_.loc[np.isin(scenario_.year,years),'wlvl median']
            elif len(years) == 1 and len(scens) == 1:
                s = scenarios[scens[0]]
                w = s.loc[s.year == years[0],'wlvl median'].values[0]
                wlvls = [w]

        # now there should be someting in wlvls
        if len(wlvls) == 1:
            # hack to set a different reference warming level for variables that don't have the 1986-2006 reference period
            if (details['source'] == 'climada' or var in ['flddph_720x360','fldfrc_720x360','flddph','fldfrc','ec2_720x360','ec2','ec3_720x360','ec3']):
                wlvls.append('1.1')
            else:
                wlvls.append('0.61')
        elif len(wlvls) == 2:
            for i,w in enumerate(wlvls):
                if w == '1986-2006':
                    wlvls[i] = '0.61'

        wlvls = sorted(wlvls)
        print(wlvls)
        wlvl_low,wlvl_high = wlvls[0], wlvls[1]

        # treatment of high-res data for climada data if available
        if var in ['ec3', 'ec2', 'flddph', 'fldfrc']:
            # if a high-res mask exists and country is not large -> use it
            # here we could also filter with a list of small countries
            fl = '/home/tooli/impact_data_explorer/data//masks/'+iso+'/masks/'+iso+'_3334x8640lat-55p77092139976058to83p1038564lon-179p979to179p97875739937945_simplify0.05_overlap.nc4'
            cou = [cou for cou in meta['countries'] if cou['id'] == iso][0]
            if cou['large'] == False and os.path.isfile(fl):
                mask = xr.load_dataset(fl)[iso]
                grid_version = ''
                lon_step, lat_step = np.diff(mask.lon,1).mean(), np.diff(mask.lat,1).mean()
                lon_shift, lat_shift = 0.25,0.25
            else:
                mask = xr.load_dataset(data_path+'/masks/'+iso+'/masks/'+iso+'_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4')[iso]
                grid_version = '_720x360'
                lon_step, lat_step = 0.5,0.5
                lon_shift, lat_shift = 0,0

        # all variables with 0.5x0.5 grid would come here
        else:
            mask = xr.load_dataset(data_path+'/masks/'+iso+'/masks/'+iso+'_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4')[iso]
            grid_version = ''
            lon_step, lat_step = 0.5,0.5
            lon_shift, lat_shift = 0,0

        # # ignore lon shift if data is downloaded
        # if mode in ['csv', 'csvModelAgreement']:
        #     lon_shift, lat_shift = 0,0

        # find the longitudes and latitudes that are relavent for the country
        lons = mask.lon[np.isfinite(np.nanmean(mask, axis=0))]
        lats = mask.lat[np.isfinite(np.nanmean(mask, axis=1))]
        mask = mask.loc[lats,lons]

        # this is hack which is required because filenames have 1986-2006 as reference period instead of 0.61
        if wlvl_low == '0.61':
            wlvl_low = '1986-2006'

        # crop global map to country extent
        nc = xr.load_dataset(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+grid_version+'_'+wlvl_high+'_'+wlvl_low+'_'+season+'.nc')
        # TODO find out why mask and data grids are not the same for flddph
        # data = nc['var'].loc[lats,lons]
        data = nc['var'].sel({'lat':lats,'lon':lons}, method='nearest')
        data = data.assign_coords(lon=data.lon.values-0.25).assign_coords(lat=data.lat.values-0.25)

        # same with model agreement
        # TODO same here
        # agreement = xr.load_dataset(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+grid_version+'_'+wlvl_high+'_'+wlvl_low+'_'+season+'.nc')['model_agreement'].loc[lats,lons]
        agreement = nc['model_agreement'].sel({'lat':lats,'lon':lons}, method='nearest')
        agreement = agreement.assign_coords(lon=agreement.lon.values-0.25).assign_coords(lat=agreement.lat.values-0.25)

        # define regions to be hatched
        agreement.values[np.isfinite(agreement.values)==False] = 0
        agreement.values[agreement.values<0.66] = 0
        agreement.values[agreement.values>=0.66] = 1

        # mask grid cells if something does not make sense
        # this is the case for runoff and dischareg and dry grid-cells for example
        if os.path.isfile(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+'_ref_mask.nc'):
            if var == 'qs':
                add_mask = xr.open_dataset(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+'_ref_mask.nc')[season].loc[lats,lons]
                data *= add_mask.values
            else:
                add_mask = xr.open_dataset(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+'_ref_mask.nc')['annual'].loc[lats,lons]
                data *= add_mask.values

        # additionally cap to 100% changes
        if details['unit'] == 'relative_change_[%]':
            data.values[data.values > 100] = 100
            data.values[data.values < -100] = -100

        # identify nans and grid-cells outside the country -> these are going to be set as null later on
        nans = np.where(~np.isfinite(data.values) | ~np.isfinite(mask.values))

        # check maps of all warming levels to define the colorbar
        if wlvl_low in ['1986-2006','1.1']:
            xtremes = np.array([])
            for wlvl in [w for w in ['1.5','2.0','2.5','3.0'] if w != wlvl_low]:
                tmp = xr.load_dataset(data_path+'/wlvl_diff/'+var+grid_version+'/'+var+grid_version+'_'+wlvl+'_'+wlvl_low+'_'+season+'.nc')['var'].sel({'lat':lats,'lon':lons}, method='nearest')
                tmp.values[np.isfinite(mask.values) == False] = np.nan
                # crop relative changes
                if details['change_type'] == 'relative':
                    tmp.values[tmp.values > 100] = 100
                    tmp.values[tmp.values < -100] = -100
                xtremes = np.append(xtremes,np.nanpercentile(tmp, [5,95]))
                xtremes = xtremes[np.isfinite(xtremes)]
            color_range = [np.nanmin(xtremes),np.nanmax(xtremes)]
        else:
            color_range = list(np.nanpercentile(data.values, [5,95]))
            # color_range = [float(np.nanmin(data.values)),float(np.nanmax(data.values))]

        # raise an error if the colorbar is not finite
        print(color_range)
        assert np.all(np.isfinite(color_range)), 'color range is not finite %s - %s' %(color_range[0], color_range[1])


        # this is for the download
        if mode == 'csv':
            data.values[nans] = np.nan
            # shift coordinates
            data = data.assign_coords(lon = data.lon + lon_shift)
            data = data.assign_coords(lat = data.lat + lat_shift)
            # convert to table
            table = data.to_pandas()
            s = io.StringIO()
            table.to_csv(s)
            header = '\n'.join(['Variable: ,'+details['name'],\
                                'Variable id: ,'+var, \
                                'Country: ,'+iso, \
                                'warming level,%s' %(wlvl_high), \
                                'reference,%s' %(wlvl_low), \
                                'difference,%s' %(details['change_type']), \
                                'GCM - GIM combinations: ,'+','.join(nc.attrs['GCM-IM input']), \
                                'All used runs: ,'+','.join(nc.attrs['GCM-IM-RCP available'])])
            s = header+'\n\n\nlat \ lon,' + ','.join(s.getvalue().split(',')[1:])
            output = make_response(s)
            output.headers["Content-Disposition"] = "attachment; filename="+'_'.join([var,iso,wlvl_high,'vs',wlvl_low,season])+".csv"
            output.headers["Content-type"] = "text/csv"
            return output

        # this is for the download
        elif mode == 'csvModelAgreement':
            data = agreement
            # shift coordinates
            data = data.assign_coords(lon = data.lon + lon_shift)
            data = data.assign_coords(lat = data.lat + lat_shift)
            # convert to table
            data.values[nans] = np.nan
            table = data.to_pandas()
            s = io.StringIO()
            table.to_csv(s)
            header = '\n'.join(['Model agreement,66% agreement -> 1,else -> 0',\
                                'Variable: ,'+details['name'],\
                                'Variable id: ,'+var, \
                                'Country: ,'+iso, \
                                'warming level,%s' %(wlvl_high), \
                                'reference,%s' %(wlvl_low), \
                                'difference,%s' %(details['change_type']), \
                                'GCM - GIM combinations: ,'+','.join(nc.attrs['GCM-IM input']), \
                                'All used runs: ,'+','.join(nc.attrs['GCM-IM-RCP available'])])
            s = header+'\n\n\nlat \ lon,' + ','.join(s.getvalue().split(',')[1:])
            output = make_response(s)
            output.headers["Content-Disposition"] = "attachment; filename="+'_'.join([var,iso,wlvl_high,'vs',wlvl_low,season])+"_modelAgreement.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        # this is a json that the website needs

        else:
            data = data.to_dict()
            agreement_tmp = agreement.to_dict()
            data['agreement'] = agreement_tmp['data']
            for i,j in zip(nans[0],nans[1]):
                data['data'][i][j] = None
                data['agreement'][i][j] = None
            data['color_range'] = color_range

            data['coords']['lon']['data'] = [x + lon_shift for x in data['coords']['lon']['data'] + [data['coords']['lon']['data'][-1] + lon_step]]
            data['coords']['lat']['data'] = [y + lat_shift for y in data['coords']['lat']['data'] + [data['coords']['lat']['data'][-1] + lat_step]]

            data['wlvl_low'] = wlvl_low
            data['wlvl_high'] = wlvl_high
            # data['wlvls'] = [wlvl_high,wlvl_low]
            data['wlvls'] = '%s vs %s' %(wlvl_high,wlvl_low)

            return compress(data)

    # if anything goes wrong, the error is going to be logged and a dummy map is returned
    except Exception as error:
        logger.exception(error)
        return jsonify(dummy_empty_map)



def match_data_to_scenario(median_, upper_bound_, lower_bound_, scenario_, flexible=True):
    '''
    transforms impact per warming level into impact in year
    smoothes the time series

    '''
    print('_'*20)
    print('match data to scenarios')

    # prepare the output table
    out = pd.DataFrame(columns = ['year','wlvl','median','upper','lower'])
    out['year'] = np.arange(2015,2105,5)
    out['wlvl'] = [str(float(scenario_.loc[scenario_.year == yr, 'wlvl median'])) for yr in out['year']]

    # remove duplicates for the markers
    # for wlvl in ['1.5','2.0','2.5','3.0']:
        # actual_year = scenario_.loc[scenario_['wlvl median'] == wlvl,'year'].values
        # if len(actual_year) > 0:
        #     actual_year = actual_year[0]
        #     if actual_year <= 2095:
        #         yr = out.year[np.argsort(np.abs(np.array(out.year, np.float)-actual_year))[0]]
        #         out.loc[out.year == yr, 'wlvl'] = wlvl

    # find highest and lowest available wlvls
    ws = [float(w) for w in median_.wlvl[np.isfinite(median_)].values]
    lo_,hi_ = str(np.min(ws)),str(np.max(ws))

    # smoothing (interpolation)
    for yr in out.year:
        # find wlvl for the given year
        wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl median'].values[0])
        # find wlvls 0.1°C below and above that wlvl
        # if possible. if wlvl is already the highest or lowest wlvl (hi_, lo_) there is notheing higher or lower
        wlvls = [str(round(w,1)) for w in [wlvl-0.1,wlvl,wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
        # take the mean of these 3 warming levels
        out.loc[out.year == yr,'median'] = float(median_.loc[wlvls].mean('wlvl'))

    if flexible:
        for yr in out.year:
            # find wlvl correpsonding to the upper bound of the scenario
            upper_wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl p95'].values[0])
            # find wlvls 0.1°C below and above that wlvl
            upper_wlvls = [str(round(w,1)) for w in [upper_wlvl-0.1,upper_wlvl,upper_wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
            # in case the wlvl is out of range -> take hi_
            if len(upper_wlvls) == 0:
                upper_wlvls = [hi_]

            # similar for the lower bound
            lower_wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl p5'].values[0])
            lower_wlvls = [str(round(w,1)) for w in [lower_wlvl-0.1,lower_wlvl,lower_wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
            if len(lower_wlvls) == 0:
                lower_wlvls = [lo_]

            # similar for the lower bound
            wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl median'].values[0])
            wlvls = [str(round(w,1)) for w in [wlvl-0.1,wlvl,wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
            if len(wlvls) == 0:
                wlvls = lower_wlvls

            # collect all possible combinations of upper lower bound scenario and upper lower bound impact to identify the extreme bounds
            possible_combis = []
            for wlvls_ in [upper_wlvls,lower_wlvls,wlvls]:
                for bound_ in [upper_bound_,lower_bound_,median_]:
                    if np.sum([w in bound_.wlvl for w in wlvls_]) > 0:
                        v = float(bound_.loc[wlvls_].mean('wlvl'))
                        if np.isfinite(v):
                            possible_combis.append(v)
            # identify upper and lower bound of all possible combinations
            if len(possible_combis) > 0:
                out.loc[out.year == yr,'upper'] = np.nanmax(possible_combis)
                out.loc[out.year == yr,'lower'] = np.nanmin(possible_combis)

        return out

    else:
        # clarify whether higher wlvls bring more or less impacts
        if median_.loc['1.7':].values.mean() - median_.loc[:'1.7'].values.mean():
            upper_ = upper_bound_
            lower_ = lower_bound_
        else:
            upper_ = lower_bound_
            lower_ = upper_bound_


        for yr in out.year:
            # find wlvl correpsonding to the upper bound of the scenario
            upper_wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl p95'].values[0])
            # find wlvls 0.1°C below and above that wlvl 
            upper_wlvls = [str(round(w,1)) for w in [upper_wlvl-0.1,upper_wlvl,upper_wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
            # in case the wlvl is out of range -> take hi_
            if len(upper_wlvls) == 0:
                upper_wlvls = [hi_]

            # similar for the lower bound
            lower_wlvl = float(scenario_.loc[scenario_.year==yr, 'wlvl p5'].values[0])
            lower_wlvls = [str(round(w,1)) for w in [lower_wlvl-0.1,lower_wlvl,lower_wlvl+0.1] if w >= float(lo_) and w <= float(hi_)]
            if len(lower_wlvls) == 0:
                lower_wlvls = [lo_]

            # collect all possible combinations of upper lower bound scenario and upper lower bound impact to identify the extreme bounds
            for name, wlvls_, bound_ in zip(['upper','lower'],[upper_wlvls,lower_wlvls],[upper_,lower_]):
                if np.sum([w in lower_.wlvl for w in wlvls_]) > 0:
                    v = float(bound_.loc[wlvls_].mean('wlvl'))
                    if np.isfinite(v):
                        out.loc[out.year == yr,name] = v

        return out




@app.route('/api/ts/', methods=['GET'])
def api_areaAverage():
    print('_'*20)
    print('areaAverage')
    print(request.args)

    try:
        iso = request.args.get('iso', default='DEU', type=None)
        region = request.args.get('region', default='DEU', type=None)
        var = request.args.get('var', default='prAdjust', type=None)
        season = request.args.get('season', default='annual', type=None)
        scenario = request.args.get('scenario', default='cat_current', type=None)
        aggregation_spatial = request.args.get('aggregation_spatial', default='area', type=None)
        mode = request.args.get('format', default='json', type=None)
    except:
        error_message = '''
        some arguments are missing.
        please provide:
        iso=???&region=???&scenario=???&var=???&season=???&aggregation_spatial=???&format=csv|json
        '''
        return error_message

    print(iso,region,var,scenario,season,aggregation_spatial,mode)

    '''
    iso = 'AUS'
    region = 'AUS'
    var = 'tasAdjust'
    scenario = 'h_cpol'
    season = 'annual'
    aggregation_spatial = 'area'
    mode = 'json'
    '''

    try:
        # don|t show predefined countries
        if var in meta['exclude_countries'].keys():
            assert iso not in meta['exclude_countries'][var], 'we excluded this country for this variable'
        
        # get details about the varaible
        details = [d for d in meta['vars'] if d['id'] == var][0]

        # hack: for climada only other as spatial aggregation is possible
        # this should become obsolote 
        if details['source'] == 'CLIMADA':
            aggregation_spatial = 'other'

        # hack for flddph and fldfrc
        if var in ['flddph','fldfrc']:
            var += '_720x360'

        # load impact for warming levels
        nc = xr.load_dataset(data_path+'/cou_data/'+var+'/'+iso+'_'+var+'.nc')
        median_ = nc['median'].sel({'region':region, 'aggregation':aggregation_spatial, 'season':season})
        upper_bound_ = (nc['upper_bound'] + nc['median']).sel({'region':region, 'aggregation':aggregation_spatial, 'season':season})
        lower_bound_ = (nc['lower_bound'] + nc['median']).sel({'region':region, 'aggregation':aggregation_spatial, 'season':season})

        if mode == 'raw':
            out = pd.DataFrame()
            out['warming level'] = nc['wlvl']
            out['median'] = median_
            out['5th percentile'] = lower_bound_
            out['95th percentile'] = upper_bound_


            s = io.StringIO()
            out.to_csv(s)
            header = '\n'.join(['Variable: ,'+details['name'],\
                                'Variable id: ,'+var, \
                                'Country: ,'+iso])
            s = header + '\n\n' + s.getvalue()
            output = make_response(s)
            output.headers["Content-Disposition"] = "attachment; filename="+'_'.join([var,scenario,iso,region,aggregation_spatial,season])+"_raw.csv"
            output.headers["Content-type"] = "text/csv"
            return output

        if mode == 'csv':
            # for download all scenarios are processed
            outs = {}
            for scenario_id,scenario_ in scenarios.items():
                if [sc for sc in meta['scenarios'] if sc['id'] == scenario_id][0]['primary']:
                    print(scenario_id)
                    tmp = match_data_to_scenario(median_=median_, upper_bound_=upper_bound_, lower_bound_=lower_bound_, scenario_=scenario_)
                    if details['change_type'] == 'relative':
                        # cap bounds to 1000% and -1000%
                        tmp.loc[tmp['lower'] < -100, 'lower'] = -100
                    name = [s for s in scenarios_meta if s['id'] == scenario_id][0]['name']
                    tmp.columns = ['year', name+' warming level', name+' median', name+' 97.5th percentile', name+' 2.5th percentile']
                    outs[scenario_id] = tmp

            out = outs['cat_current']
            for scenario_id in [s for s in scenarios.keys() if s != 'cat_current' and 'rcp' not in s]:
                for c_name in outs[scenario_id].columns[1:]:
                    out[c_name] = outs[scenario_id][c_name]
            for scenario_id in [s for s in scenarios.keys() if 'rcp' in s]:
                for c_name in outs[scenario_id].columns[1:]:
                    out[c_name] = outs[scenario_id][c_name]

            #out = out.loc[[type(w) == str for w in out.wlvl]]
            s = io.StringIO()
            out.to_csv(s)
            header = '\n'.join(['Variable: ,'+details['name'],\
                                'Variable id: ,'+var, \
                                'Country: ,'+iso])
            s = header + '\n\n' + s.getvalue()
            output = make_response(s)
            output.headers["Content-Disposition"] = "attachment; filename="+'_'.join([var,iso,region,aggregation_spatial,season])+".csv"
            output.headers["Content-type"] = "text/csv"
            return output

        if mode == 'json':
            # get projection
            scenario_ = scenarios[scenario]

            # find the matching warming levels for years, smooth and get full uncertanity
            out = match_data_to_scenario(median_=median_, upper_bound_=upper_bound_, lower_bound_=lower_bound_, scenario_=scenario_)

            # filter out extreme changes
            # print(np.array(out.iloc[:,1:].values.flatten(),np.float))
            xtremes = np.nanmax(np.abs(np.array(out['median'].values.flatten(),float)))
            if details['change_type'] == 'relative':
                assert xtremes < 10**3, 'extreme changes %s \n median changes\n' %(xtremes) + '\n'.join([str(w)+':'+str(x) for w,x in zip(out['wlvl'],out['median'])])

                # cap bounds to 1000% and -1000%
                out.loc[out['lower'] < -100, 'lower'] = -100
                out.loc[out['upper'] > 1000, 'upper'] = 1000


            # filter out missing data
            out = out.loc[np.isfinite(np.array(out['median'], float))]

            # filter out wlvl marker duplicates
            tmp = out['wlvl'].values
            w_ = '0'
            for i,w in enumerate(tmp):
                if w != w_:
                    w_ = w
                else:
                    tmp[i] = np.nan
            out['wlvl'] = tmp


            # get value of reference period
            if details['group'] == 'Climate':
                ref = float(xr.load_dataset(data_path+'/cou_data/'+var+'/'+iso+'_'+var+'_ref.nc')['ref'].sel({'region':region, 'aggregation':aggregation_spatial, 'season':season}).median('ID').values)
                if 'tas' in var:
                    ref -= 273.15
                ref = round(ref,2)
            else:
                ref = None

            # transform to dict
            out_dict = {
                'year' : [float(yr) for yr in out.year.values],
                'reference_value' : ref,
            }

            # replace nan's by None (-> being transformed to null by jsonify)
            for name in ['wlvl','median','lower','upper']:
                tmp = np.array(out.loc[:,name].values.squeeze(), float)
                nans = np.where(np.isnan(tmp))[0]
                tmp = list(tmp)
                for i in nans:
                    tmp[i] = None
                out_dict[name] = tmp

            # replace duplicate markers
            tmp = np.array(out_dict['wlvl'])
            for wlvl in ['1.5','2.0','2.5','3.0']:
                if np.sum(tmp == float(wlvl)) > 1:
                    for i in np.where(tmp == float(wlvl))[0][1:]:
                        out_dict['wlvl'][i] = None
                elif np.sum(tmp == float(wlvl)) == 0:
                    if len([ww for ww in [w for w in tmp if w is not None] if ww > float(wlvl)]) > 0:
                        for i,w in enumerate(tmp):
                            if w is not None:
                                if float(w) > float(wlvl):
                                    out_dict['wlvl'][i] = float(wlvl)
                                    break
                                    # out_dict['wlvl'][np.where(tmp_ > float(wlvl))[0][0]] = float(wlvl)

            print(out_dict['wlvl'])

            return jsonify(out_dict)

    except Exception as error:
        logger.exception(error)
        return jsonify(dummy_empty_ts)




if __name__ == "__main__":
    app.run(host='0.0.0.0')
    
# @app.route('/api/maps/', methods=['GET'])
# def api_maps():
#     print(request.args)
#     try:
#         iso = request.args.get('iso', default='DEU', type=str)
#         wlvl_low = request.args.get('wlvl_low', default='1986-2006', type=str)
#         wlvl_high = request.args.get('wlvl_high', default='2.0', type=str)
#         var = request.args.get('var', default='prAdjust', type=str)
#         season = request.args.get('season', default='annual', type=str)
#         mode = request.args.get('format', default='json', type=str)
#     except:
#         error_message = '''
#         some arguments are missing.
#         please provide:
#         ?iso=???&wlvl_high=???&wlvl_low=???&var=???&season=???&format=csv|json
#         '''
#         return error_message

#     '''
#     iso = 'DEU'
#     wlvl_low = '1.5'
#     wlvl_high = '3.0'
#     var = 'ec2'
#     season = 'annual'
#     mode = 'json'
#     '''

#     # I use try - except here to avoid the app to crash
#     # if something goes wrong, a dummy response is given out and the error is written in a log file
#     try:
#         # load details about the variable
#         # these are similar to the information in the google sheet
#         details = [d for d in meta['vars'] if d['id'] == var][0]

#         # hack to set a different reference warming level for variables that don't have the 1986-2006 reference period
#         if (details['source'] == 'CLIMADA' or var in ['flddph_720x360','fldfrc_720x360']) and wlvl_low == '1986-2006':
#             wlvl_low = '1.1'

#         return diff_map(details, wlvl_high, wlvl_low, iso, var, season, mode)


#     # if anything goes wrong, the error is going to be logged and a dummy map is returned
#     except Exception as error:
#         logger.exception(error)
#         return jsonify(dummy_empty_map)

