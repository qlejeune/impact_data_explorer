

dummy_empty_map = {
  "agreement": None,
  "attrs": {}, 
  "coords": {
    "ID": {
      "attrs": {}, 
      "data": "HadGEM2-ES_rcp60", 
      "dims": []
    }, 
    "lat": {
      "attrs": {
        "axis": "Y", 
        "long_name": "latitude", 
        "standard_name": "latitude", 
        "units": "degrees_north"
      }, 
      "data": [None,None], 
      "dims": [
        "lat"
      ]
    }, 
    "lon": {
      "attrs": {
        "axis": "X", 
        "long_name": "longitude", 
        "standard_name": "longitude", 
        "units": "degrees_east"
      }, 
      "data": [None, None, None,], 
      "dims": [
        "lon"
      ]
    }, 
    "region": {
      "attrs": {}, 
      "data": "BEN", 
      "dims": []
    }
  }, 
  "data": None, 
  "dims": [
    "lat", 
    "lon"
  ], 
  "name": "var"}


dummy_empty_ts = {
			"lower": None, 
			"median": None, 
			"upper": None, 
			"wlvl": None, 
			"year": None, 
			}