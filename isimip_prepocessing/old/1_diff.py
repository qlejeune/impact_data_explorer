import os,sys, importlib, glob,gc
import operator

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

# some functions that are also used by 1_glob_warming_level_difference.py
sys.path.append('isimip_prepocessing')
exec("import %s; importlib.reload(%s); from %s import *" % tuple(['_helping_functions']*3))

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=[])
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['yield_maize_co2'])

parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=[str(w) for w in np.arange(1.0,5.1,0.1).round(1)])

parser.add_argument('-p','--plotCous', help='list of countries for which to create plots', nargs='+', default=['DEU','BEN','CHN'])

parser.add_argument('--globDiffMap', dest='globDiffMap', action='store_true')
parser.add_argument('--no-globDiffMap', dest='globDiffMap', action='store_false')
parser.set_defaults(globDiffMap=True)

parser.add_argument('--couAv', dest='couAv', action='store_true')
parser.add_argument('--no-couAv', dest='couAv', action='store_false')
parser.set_defaults(couAv=True)

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=True)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# in case no iso was specified -> take all isos
if len(args['isos']) == 0:
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = [info.attributes['adm0_a3'] for info in cous]
else:
    isos = args['isos']

# dict { season name : list(months) }
season_dict = {
    'annual' : list(range(1,13)),
    'DJF' : [1,2,12],
    'MAM' : [3,4,5],
    'JJA' : [6,7,8],
    'SON' : [9,10,11],
}

# this is goinf to be initialized with the first wlvl
iso_dict = {}

for var in args['vars']:
    print(var)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    if 'excludeImpModels' in list(details.keys()):
        exludeImp = details['excludeImpModels']
    else:
        exludeImp = []

    # here I load reference slices
    # I load them several times - once for each RCP
    # I need this to be able to easily calculate changes later on
    files_all = glob.glob(details['wlvl_files_pattern'])
    files = [f for f in files_all if '1986-2006' in f]
    # filter out excluded imp models
    files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
    ref = []
    for rcp in ['rcp26','rcp45','rcp60','rcp85']:
        # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
        IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'').replace('historical',rcp) for f in files]
        tmp = xr.open_mfdataset(files, concat_dim='ID')[var]#.sel({'lat':lats, 'lon':lons})
        tmp = tmp.assign_coords(ID=IDs)
        ref.append(tmp)
    ref = xr.concat(ref, dim='ID')

    # load the data into the RAM
    ref.load()

    for wlvl in args['wlvls']:
        # this might free RAM during the loops
        print(wlvl)

        # here I load all files for the warming level
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if wlvl in f]
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
        IDs = [f.split('/')[-1].replace('_'+wlvl+'.nc','').replace('_'+var,'') for f in files]
        proj = xr.open_mfdataset(files, concat_dim='ID')[var]#.sel({'lat':lats, 'lon':lons})
        proj = proj.assign_coords(ID=IDs)
    
        # load into RAM
        proj.load()

        # this is a correction required for some specific data (led, ped, .. etc.)
        if 'correction_factor' in details.keys():
            ref *= float(details['correction_factor'])
            proj *= float(details['correction_factor'])

        # here I make a selection of IDs that are available in ref and proj
        IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]

        # this will be required later when averageing over RCPs
        IDs_selected, IDs_gcm_im = select_GCM_IM_combinations(IDs=IDs, excludeImpModels=exludeImp)

        ###########################
        # wlvl diff global fields
        ###########################
        if args['globDiffMap']:
            # create output path it does not exist
            path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
            if os.path.isdir(path_out) == False:
                os.system('mkdir -p '+path_out)

            # there is one map per seasonal aggreagtion
            for season,months in season_dict.items():
                if details['temporal_resolution'] == 'yearly' and season != 'annual':
                    pass
                else:
                    out_file = path_out+'_'.join([var,wlvl,ref_wlvl,season])+'.nc'
                    if os.path.isfile(out_file) == False or args['overwrite']:
                        if details['temporal_resolution'] == 'yearly':
                            tmp_proj,tmp_ref = proj,ref
                        else:
                            tmp_ref = ref.sel(month=months).reduce(details['seasonal_aggreagation'], 'month')
                            tmp_proj = proj.sel(month=months).reduce(details['seasonal_aggreagation'], 'month')

                        # this is now the change in the the var for a warming level for all different gcm-impModel-rcp combinations
                        if details['rel-abs'] == 'relative':
                            diff_all = ( tmp_proj.sel(ID=IDs) - tmp_ref.sel(ID=IDs) ) / tmp_ref.sel(ID=IDs) * 100
                        if details['rel-abs'] == 'absolute':
                            diff_all = tmp_proj.sel(ID=IDs) - tmp_ref.sel(ID=IDs)

                        # mask irrealistic grid cells.
                        # required for crop yields
                        if 'irrealistic_values' in details.keys():
                            diff_all.values[details['irrealistic_values']['operator'](diff.values, details['irrealistic_values']['threshold'])] = np.nan

                        # average over rcps
                        diff = average_over_rcps(diff_all=diff_all, IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

                        # get the ensemble median
                        ens_median = diff.median('ID')

                        # model agreement
                        model_agreement = ens_median.copy() * 0.0
                        for ID in diff.ID.values:
                            model_agreement += np.sign(diff.loc[ID]) == np.sign(ens_median)
                        model_agreement /= float(len(IDs_selected))

                        ds = xr.Dataset({'var':ens_median, 'model_agreement':model_agreement})
                        ds.attrs = {'GCM-IM input':IDs_selected, 'GCM-IM-RCP available':list(IDs)}
                        ds.to_netcdf(out_file)
                        gc.collect()

        ###########################
        # wlvl diff global fields
        ###########################
        if args['couDiffAvs']:
            # go through countries - somehow I call them isos because I work with 3 letter country codes
            for iso in isos:
                if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
                    print(iso)
                    # create output path it does not exist
                    path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'+iso+'/'
                    if os.path.isdir(path_out) == False:
                        os.system('mkdir -p '+path_out)

                    # define output file name and only continue if that file does not exist or the option is --overwrite
                    file_out = path_out+'_'.join([iso,var,wlvl])+'_regAv.nc'
                    if os.path.isfile(file_out) == False or args['overwrite']:

                        # the following block only happens for the first warming level
                        # it loads masks, prepares output arrays etc.
                        if iso not in iso_dict.keys():
                            # load masks
                            COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
                            COU.load_mask()

                            # put masks in new dict
                            mask_dict = {
                                'area' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['latWeight'],
                                'pop' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['pop2005'],
                                'gdp' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['gdp2005'],
                            }
                            # I'll take lons and lats from this mask
                            mask_overlap = COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['overlap']

                            # depending on --subregions or --no-subregions the region names are loaded
                            if args['subregions']:
                                region_names = mask_overlap.coords['region'].values
                            else:
                                region_names = [iso]

                            # in this array I will store deviations from the median
                            deviation = xr.DataArray(coords={'region':region_names, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys()),'ID':np.unique(IDs_gcm_im)}, dims=['region','aggregation','season','ID'])

                            # all details are stored in the iso_dict
                            iso_dict[iso] = {
                                'lats' : mask_overlap.coords['lat'].values,
                                'lons' : mask_overlap.coords['lon'].values,
                                'diff' : deviation,
                                'mask_dict' : mask_dict,
                                'region_names' : region_names
                            }


                        # get details for the iso
                        iso_details = iso_dict[iso]

                        for aggr,masks in iso_details['mask_dict'].items():
                            for region in iso_details['region_names']:

                                # select mask
                                mask = masks.loc[region]

                                # average over area
                                ref_area = (ref.sel(ID=IDs).sel({'lat':iso_details['lats'], 'lon':iso_details['lons']}) * mask).sum('lat').sum('lon')
                                proj_area = (proj.sel(ID=IDs).sel({'lat':iso_details['lats'], 'lon':iso_details['lons']}) * mask).sum('lat').sum('lon')

                                # the next block is a simplification for annual data -> check the "else" block for details
                                if details['temporal_resolution'] == 'yearly':
                                    ref_area_seas = ref_area
                                    proj_area_seas = proj_area

                                    if details['rel-abs'] == 'relative':
                                        diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
                                    if details['rel-abs'] == 'absolute':
                                        diff_all = proj_area_seas - ref_area_seas

                                    diff_all = diff_all.load()
                                    # mask irrealistic grid cells.
                                    # required for crop yields
                                    if 'irrealistic_values' in details.keys():
                                        diff_all.values[details['irrealistic_values']['operator'](diff.values, details['irrealistic_values']['threshold'])] = np.nan

                                    # average over rcps
                                    diff = average_over_rcps(diff_all=diff_all, IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

                                    iso_details['diff'].loc[region,aggr,'annual',diff.ID] = diff

                                else:
                                    for season,months in season_dict.items():
                                        # select months of season and aggregate depending on the "seasonal_aggregation" in details
                                        ref_area_seas = ref_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')
                                        proj_area_seas = proj_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')

                                        # this is now the change in the the variable for a warming level for all different gcm-impModel-rcp combinations
                                        if details['rel-abs'] == 'relative':
                                            diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
                                        if details['rel-abs'] == 'absolute':
                                            diff_all = proj_area_seas - ref_area_seas

                                        # load into RAM if it isn't already there
                                        diff_all = diff_all.load()

                                        # average over rcps
                                        diff = average_over_rcps(diff_all=diff_all, IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

                                        iso_details['diff'].loc[region,aggr,season,diff.ID] = diff
                                        
                        xr.Dataset({'diff':iso_details['diff']}).to_netcdf(file_out)
                        print('done',wlvl,iso,file_out)













