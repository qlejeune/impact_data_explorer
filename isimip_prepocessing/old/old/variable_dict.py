import os,sys, importlib, glob,gc


variable_dict = {
	'maxdis' : {
		'files' : glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/maxdis/*/*/*/*'),
		'name' : 'maxdis',
		'orig_name' : 'dis',
		'temporal_aggreagation' : np.max
	},
	'pr' : {
		'files' : glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/pr/*/*/*'),
		'name' : 'pr',
		'orig_name' : 'pr',
		'temporal_aggreagation' : np.sum
	},
	'tasmax' : {
		'files' : glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/tasmax/*/*/*'),
		'name' : 'tasmax',
		'orig_name' : 'tasmax',
		'temporal_aggreagation' : np.max
	},
	'tas' : {
		'files' : glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/tas/*/*/*'),
		'name' : 'tas',
		'orig_name' : 'tas',
		'temporal_aggreagation' : np.mean
	},
	'sfcWind' : {
		'files' : glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/sfcWind/*/*/*'),
		'name' : 'sfcWind',
		'orig_name' : 'sfcWind',
		'temporal_aggreagation' : np.mean
	},
}

#
