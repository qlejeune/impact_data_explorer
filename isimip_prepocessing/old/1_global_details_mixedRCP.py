import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl

import cartopy.crs as ccrs
import cartopy

# some functions that are also used by 2_cou_data_1_regional_averages.py.py
sys.path.append('isimip_prepocessing')
exec("import %s; importlib.reload(%s); from %s import *" % tuple(['_helping_functions']*3))

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['led'])
parser.add_argument('-wl','--wLow', help='low warming level', default='1986-2006')
parser.add_argument('-wh','--wHigh', help='high warming level', default='2.0')

parser.add_argument('-f','--factor', help='factor', default=100.0, type=float)

parser.add_argument('-c','--colorScale', help='colorScale', nargs='+', default=[-6,6])
parser.add_argument('-ct','--colorType', help='colorType', default='RdBu_r')

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

season_dict = {
    'annual' : list(range(1,13)),
    'DJF' : [1,2,12],
    'MAM' : [3,4,5],
    'JJA' : [6,7,8],
    'SON' : [9,10,11],
}

norm = mpl.colors.Normalize(0,1)
cmap1 = mpl.colors.LinearSegmentedColormap.from_list("", ['yellow','red','m'])
cmap2 = mpl.colors.LinearSegmentedColormap.from_list("", ['white','green','blue'])
colors = np.vstack((cmap2(np.linspace(0, 1, 66)), cmap1(np.linspace(0, 1, 33))))
cmap_agree = mpl.colors.LinearSegmentedColormap.from_list('mixed_cmap', colors)


def model_agreement(one_,all_,gcms,ims):
    agree = one_.copy() * 0
    count = 0
    for gcm in gcms:
        for im in ims:
            if np.any(np.isfinite(all_.loc[gcm,im])):
                agree += np.sign(one_) == np.sign(all_.loc[gcm,im])
                count += 1
    # agree.values[agree.values<=count*2/3] = 1
    # agree.values[agree.values>count*2/3] = np.nan
    agree /= float(count)
    return agree



for var in args['vars']:
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    if 'excludeImpModels' in list(details.keys()):
        exludeImp = details['excludeImpModels']
    else:
        exludeImp = []

    path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
    os.system('mkdir -p '+path_out)

    files_all = glob.glob(details['wlvl_files_pattern'])
    # filter out excluded imp models
    files_all = [f for f in files_all if len([i for i in exludeImp if i in f]) == 0]
    files_high = [f for f in files_all if args['wHigh'] in f]
    files_low = [f for f in files_all if args['wLow'] in f]
    
    ims = np.unique([fl.split('_')[-3] for fl in files_high])
    gcms = np.unique([fl.split('/')[7] for fl in files_high])

    plt.close('all')
    fig,axes = plt.subplots(nrows=len(gcms)+4, ncols=len(ims)+3, figsize=(len(ims)*4,len(gcms)*2),subplot_kw={'projection': ccrs.PlateCarree()}, gridspec_kw={'height_ratios':[0.5]+[6]*(len(gcms)+2)+[0.7], 'width_ratios':[0.5] + [8] * (len(ims)+2)})
    for r,gcm in enumerate(gcms):
        for c,im in enumerate(ims):	
            print(im)
            ax = axes[r+1,c+1]
            high = [fl for fl in files_high if im in fl and gcm in fl]
            low = [fl for fl in files_low if im in fl and gcm in fl]
            
            if len(low) > 1:
                asdas
            else:
                low = xr.open_dataset(low[0])[var]
                to_plot = low.copy() * 0
                for fl in high:
                    to_plot += xr.open_dataset(fl)[var] - low
                to_plot /= float(len(high))

            to_plot.values *= args['factor']
            if np.any(np.isfinite(to_plot)):
                ax.coastlines(linewidth=0.05)
                ax.pcolormesh(to_plot.lon, to_plot.lat, to_plot, cmap=args['colorType'], vmin=args['colorScale'][0], vmax=args['colorScale'][1], transform=ccrs.PlateCarree())
            else:
                ax.outline_patch.set_edgecolor('white')
                ax.annotate('no data',xy=(0.5,0.5), xycoords='axes fraction', ha='center',va='center')

            if r+c == 0:
                diff_all = xr.DataArray(coords={'gcm':gcms, 'im':ims, 'lat':to_plot.lat, 'lon':to_plot.lon}, dims=['gcm','im','lat','lon'])

            diff_all.loc[gcm,im] = to_plot.values

        med = diff_all.loc[gcm].median('im')
        if np.any(np.isfinite(med)):
            axes[r+1,-2].coastlines(linewidth=0.05)
            img = axes[r+1,-2].pcolormesh(to_plot.lon, to_plot.lat, med, cmap=args['colorType'], vmin=args['colorScale'][0], vmax=args['colorScale'][1], transform=ccrs.PlateCarree())

            agree = model_agreement(med,diff_all, gcms=[gcm], ims=ims)
            axes[r+1,-1].pcolormesh(to_plot.lon, to_plot.lat, agree, cmap=cmap_agree, vmin=0, vmax=1, transform=ccrs.PlateCarree())

        else:
            axes[r+1,-2].outline_patch.set_edgecolor('white')
            axes[r+1,-2].annotate('no data',xy=(0.5,0.5), xycoords='axes fraction', ha='center',va='center')
            axes[r+1,-1].outline_patch.set_edgecolor('white')
            axes[r+1,-1].annotate('no data',xy=(0.5,0.5), xycoords='axes fraction', ha='center',va='center')


    for c,im in enumerate(ims):
        med = diff_all.loc[:,im].median('gcm')
        if np.any(np.isfinite(med)):
            axes[-3,c+1].coastlines(linewidth=0.05)
            img = axes[-3,c+1].pcolormesh(to_plot.lon, to_plot.lat, med, cmap=args['colorType'], vmin=args['colorScale'][0], vmax=args['colorScale'][1], transform=ccrs.PlateCarree())

            agree = model_agreement(med,diff_all, gcms=gcms,ims=[im])
            axes[-2,c+1].pcolormesh(to_plot.lon, to_plot.lat, agree, cmap=cmap_agree, vmin=0, vmax=1, transform=ccrs.PlateCarree())

        else:
            axes[-3,c+1].outline_patch.set_edgecolor('white')
            axes[-3,c+1].annotate('no data',xy=(0.5,0.5), xycoords='axes fraction', ha='center',va='center')
            axes[-2,c+1].outline_patch.set_edgecolor('white')
            axes[-2,c+1].annotate('no data',xy=(0.5,0.5), xycoords='axes fraction', ha='center',va='center')



    axes[-3,-2].coastlines(linewidth=0.05)
    med = diff_all.median(('gcm','im'))
    axes[-3,-2].pcolormesh(to_plot.lon, to_plot.lat, med, cmap=args['colorType'], vmin=args['colorScale'][0], vmax=args['colorScale'][1], transform=ccrs.PlateCarree())

    agree = model_agreement(med,diff_all,gcms=gcms,ims=ims)
    axes[-2,-1].pcolormesh(to_plot.lon, to_plot.lat, agree, cmap=cmap_agree, vmin=0, vmax=1, transform=ccrs.PlateCarree())

    axes[0,0].outline_patch.set_edgecolor('white')
    axes[0,0].annotate(var,xy=(0.5,0.5), xycoords='axes fraction', ha='center', va='center')

    for r,gcm in enumerate(list(gcms)+['median','agree']):
        axes[r+1,0].outline_patch.set_edgecolor('white')
        axes[r+1,0].annotate(gcm,xy=(0.5,0.5), xycoords='axes fraction', ha='center', va='center')

    for c,impModel in enumerate(list(ims)+['median','agree']):
        axes[0,c+1].outline_patch.set_edgecolor('white')
        axes[0,c+1].annotate(impModel,xy=(0.5,0.5), xycoords='axes fraction', ha='center', va='center', rotation=0)

    for ax in axes[-1,:]:
        ax.outline_patch.set_edgecolor('white')

    cbar_ax=fig.add_axes([0.05,0.05,0.4,0.2])
    cbar_ax.axis('off')
    img = cbar_ax.imshow(np.array([args['colorScale']]), cmap=args['colorType'])
    img.set_visible(False)
    cb=fig.colorbar(img, orientation='horizontal',ax=cbar_ax)
    cb.set_label(label='change in water scarcity [%]')

    cbar_ax=fig.add_axes([0.55,0.05,0.4,0.2])
    cbar_ax.axis('off')
    img = cbar_ax.imshow(np.array([[0,1]]), cmap=cmap_agree)
    img.set_visible(False)
    cb=fig.colorbar(img, orientation='horizontal',ax=cbar_ax)
    cb.set_label(label='model agreement')

    plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/details_'+'_'.join([var,args['wHigh'],args['wLow']])+'.png', bbox_inches='tight', dpi=300)                  

