import os,sys, importlib, glob,gc, pickle
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json

import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.shapereader as shapereader

from shapely.ops import cascaded_union, unary_union

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

try:
    isos = [sys.argv[1]]
except:
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = [info.attributes['adm0_a3'] for info in cous] + ['ESH','SSD']
    isos = ['IDN']

try:
    overwrite = sys.argv[2]
except:
    overwrite = True

for iso in isos:
    print(iso)
    if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) == 0 or overwrite:
        print('create mask first')

        # os.system('rm -r '+'/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
        COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
        # COU.download_shapefile()


        shape0 = COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_0.shp'
        if os.path.isfile(shape0):
            COU.read_shapefile(shape0)
            COU.read_shapefile(shape0, long_name='NAME_0', short_name='GID_0')

            shape1 = COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_1.shp'
            if os.path.isfile(shape1):
                    COU.read_shapefile(shape1)
                    COU.read_shapefile(shape1, long_name='NAME_1', short_name='GID_1')
            COU.load_shapefile()


            COU.create_masks_overlap(input_file='/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/historical/HadGEM2-ES/tas_day_HadGEM2-ES_historical_r1i1p1_EWEMBI_18610101-18701231.nc4')
            COU.create_masks_latweighted()

        if iso == 'IDN':
            shape1 = COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_1.shp'
            if os.path.isfile(shape1):
                    COU.read_shapefile(shape1)
                    COU.read_shapefile(shape1, long_name='NAME_1', short_name='GID_1')
            COU.load_shapefile()

            COU._region_polygons[iso] = unary_union(COU._region_polygons.values())
            COU._region_names[iso] = 'Indonesia'

            # plot the polygons
            plt.close('all')
            fig,ax = plt.subplots(nrows=1, subplot_kw={'projection': ccrs.PlateCarree()})
            ax.coastlines()
            ax.add_geometries(COU._region_polygons.values(), ccrs.PlateCarree(), edgecolor='m',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
            ax.add_geometries([COU._region_polygons[iso]], ccrs.PlateCarree(), edgecolor='c',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
            plt.savefig(COU._working_dir+iso+'.pdf', dpi=300)

            pickle.dump(COU._region_names, open(COU._working_dir+'region_names.pkl', 'wb'), pickle.HIGHEST_PROTOCOL)
            pickle.dump(COU._region_polygons, open(COU._working_dir+'region_polygons.pkl', 'wb'), pickle.HIGHEST_PROTOCOL)

            COU.create_masks_overlap(input_file='/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/historical/HadGEM2-ES/tas_day_HadGEM2-ES_historical_r1i1p1_EWEMBI_18610101-18701231.nc4')
            COU.create_masks_latweighted()

        if iso in ['ESH','SSD']:

            COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
            COU.download_shapefile()

            shape0 = COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_0.shp'
            if os.path.isfile(shape0):
                COU.read_shapefile(shape0)
                COU.read_shapefile(shape0, long_name='NAME_0', short_name='GID_0')

                shape1 = COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_1.shp'
                if os.path.isfile(shape1):
                        COU.read_shapefile(shape1)
                        COU.read_shapefile(shape1, long_name='NAME_1', short_name='GID_1')
                COU.load_shapefile()

                COU.create_masks_overlap(input_file='/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/historical/HadGEM2-ES/tas_day_HadGEM2-ES_historical_r1i1p1_EWEMBI_18610101-18701231.nc4')
                COU.create_masks_latweighted()

                COU.load_mask()
                COU.create_masks_other_weight(pop2005, 'pop2005')
                COU.create_masks_other_weight(gdp2005, 'gdp2005')
                gc.collect()

# for iso in isos:
#     print(iso)
#     COU.create_masks_latweighted()

pop2005 = xr.open_dataset('/p/projects/isimip/isimip/ISIMIP2b/InputData/population/2005soc/population_2005soc_0p5deg_annual_2006-2099.nc4', decode_times=False)['number_of_people'][0]
pop2005.squeeze().load()
pop2005 = xr.DataArray(pop2005.values, coords={k:pop2005.coords[k] for k in ['lat','lon']}, dims=['lat','lon'])

for iso in isos:
    print(iso)
    if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
        COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
        COU.load_mask()

        COU.create_masks_other_weight(pop2005, 'pop2005')
        gc.collect()

gdp2005 = xr.open_dataset('/p/projects/isimip/isimip/ISIMIP2b/InputData/gdp/2005soc/gdp_2005soc_0p5deg_annual_2006-2099.nc4', decode_times=False)['gdp'][0]
gdp2005.squeeze().load()
gdp2005 = xr.DataArray(gdp2005.values, coords={k:gdp2005.coords[k] for k in ['lat','lon']}, dims=['lat','lon'])

for iso in isos:
    print(iso)
    if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
        COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
        COU.load_mask()

        COU.create_masks_other_weight(gdp2005, 'gdp2005')
        gc.collect()


#
