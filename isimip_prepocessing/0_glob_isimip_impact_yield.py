
import os,sys,glob,gc,importlib

import numpy as np
import xarray as xr
import pandas as pd

###############################
# this is a special version of 0_glob_isimip_impact.py
# please also see comments in there to understand
###############################

os.chdir('/home/pepflei/Projects/impact_data_explorer/')

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v','--var', help='variable', default='yield_maize_co2')
parser.add_argument('-g','--gcm', help='gcm', required=False, default=None)
parser.add_argument('-s','--scenario', help='scenario', required=False, default=None)
parser.add_argument('-m','--mode', help='only test?', required=False, default='run')
parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=[])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=False)

args = vars(parser.parse_args())

print(args)

short_name = args['var']

if args['gcm'] is not None:
    gcms = [args['gcm']]
else:
    gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']

if args['scenario'] is not None:
    scenarios = [args['scenario']]
else:
    scenarios = ['historical','rcp26','rcp45','rcp60','rcp85']

overwrite = args['overwrite']
mode = args['mode']
selected_wlvls = args['wlvls']

'''
short_name = 'ped'
gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
gcms = ['HadGEM2-ES']
scenarios = ['rcp26']
mode = 'test'
overwrite = True
selected_wlvls = []
'''

# get the details about the variable
# this will give a dict called "details" which has information about paths of input files, temporal aggregation method etc.
sys.path.append('meta/variables/')
exec("import %s; importlib.reload(%s); from %s import *" % tuple([short_name+'_check']*3))

# get all files that could be relevant in one list
if 'in_files' in details.keys():
    files_all = details['in_files']
else:
    files_all = glob.glob(details['in_files_pattern_historical']) + glob.glob(details['in_files_pattern_future'])

# get a list of impact models
impModels = np.unique([ff.split('/')[-4] for ff in files_all])

# load warming levels (wlvls) and store them in a xarray
wlvlYears_csv = pd.read_csv('meta/warming_lvls_cmip5_21_years.csv')
wlvls = np.array(wlvlYears_csv.wlvl.round(1), str)
wlvlYears = xr.DataArray(coords={'gcm':gcms, 'scenario':scenarios, 'wlvl':['1986-2006']+list(wlvls)}, dims=['gcm','scenario','wlvl'])
for gcm in gcms:
    if 'historical' in scenarios:
        wlvlYears.loc[gcm,'historical','1986-2006'] = 1996
    for scenario in [scen for scen in scenarios if scen!='historical']:
        for wlvl in wlvls:
            wlvlYears.loc[gcm,scenario,wlvls] = wlvlYears_csv[gcm+'_'+scenario]

# do it for all wlvls if no wlvls were specified
if len(selected_wlvls) == 0:
    selected_wlvls = wlvlYears.wlvl.values

print(short_name,gcms,scenarios,overwrite,mode,selected_wlvls)

# load masks
nc = xr.open_dataset(details['mask_file'])
noirr_mask = nc['rain']
firr_mask = nc['irr']

# go through gcms, rcps, wlvls, and imp models
for gcm in gcms:
    for scenario in scenarios:
        for wlvl in selected_wlvls:
            if np.isfinite(wlvlYears.loc[gcm,scenario,wlvl]):
                # get a list of years that are required for the wlvl
                years = np.arange(wlvlYears.loc[gcm,scenario,wlvl]-10,wlvlYears.loc[gcm,scenario,wlvl]+11,1)
                for impModel in impModels:

                    outPath = '/p/tmp/pepflei/impact_data_explorer_data/raw/'+'/'.join([short_name,gcm,scenario,impModel])+'/'
                    outFile = outPath+'_'.join([gcm,scenario,impModel,short_name,wlvl])+'.nc'
                    if os.path.isfile(outFile) == False or overwrite:
                        
                        # select the relevant files
                        files_sel = [ff for ff in files_all if ff.split('/')[-3] == gcm.lower()]
                        files_sel = [ff for ff in files_sel if ff.split('/')[-4] == impModel]
                        files_scen = [ff for ff in files_sel if scenario in ff.split('/')[-1]]

                        # if there are some files for this gcm-rcp-im combination
                        if len(files_scen) > 0:
                            
                            # add historical files if the period includes years before 2006
                            if years.min() < 2006 and years.max() > 2006:
                                print('requiring hist years')
                                files_hist = [ff for ff in files_sel if ff.split('/')[-1].split('_')[3] == 'historical']
                                files_scen += files_hist

                            # make a list of files that are required for the warming level
                            required_files = []
                            for file_name in files_scen:
                                start,stop = (float(fl) for fl in file_name.split('.')[0].split('_')[-2:])
                                if np.sum(np.isin(years,np.arange(start,stop+1,1))) > 0:
                                    # exclude dupliactes:
                                    # for some models output exists in water_global, biomes and permafrost
                                    # here we would only take one of these files
                                    if file_name.split('/')[-1] not in [fl.split('/')[-1] for fl in required_files]:
                                        required_files.append(file_name)

                            # split in firr and no irr
                            firr_files = [fl for fl in required_files if 'firr' in fl]
                            tmp_firr = xr.open_mfdataset(firr_files, combine='by_coords', concat_dim='time', decode_times=False)[details['orig_name']+'-firr']

                            noirr_files = [fl for fl in required_files if 'noirr' in fl]
                            tmp_noirr = xr.open_mfdataset(noirr_files, combine='by_coords', concat_dim='time', decode_times=False)[details['orig_name']+'-noirr']

                            # calculate combined yield
                            data = tmp_firr * firr_mask + tmp_noirr * noirr_mask

                            # fix time unit issues
                            time_units = data.time.attrs['units']
                            ref_year = time_units.split('years since ')[-1].split('-')[0]
                            data = data.assign_coords(time=data.time.values+int(ref_year))
                            if len(ref_year) == 4:
                                wlvlSlice = data[np.isin(data.time.values,years),:,:].mean('time')

                            if os.path.isdir(outPath) == False:
                                os.system('mkdir -p '+outPath)

                            print('writing: '+outFile)
                            # save the warming level slice
                            xr.Dataset({details['short_name']:wlvlSlice}).to_netcdf(outFile)

                            gc.collect()
                            if mode == 'test':
                                print('working ' + outFile)
                                asdas

                    else:
                        print('allready done ' + outFile)





#
