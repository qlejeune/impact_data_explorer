


# Masks

```
rsync -zarvm --include='*/' --include='*latWeight*' --exclude='*' /p/tmp/pepflei/impact_data_explorer_data/ tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorere_api/masks/
```


# Data

```
rsync -zarvm --include='*/' --include='*qtot*' --exclude='*' /p/tmp/pepflei/water tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorere_api/data/
```

# Scripts

```
scp /Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/api.py tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer_api/
scp /Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/paths_tooli.py tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer_api/paths.py

```


```
rsync -zarvm --include='*/' --include='*/wlvl_diff/*' --exclude='*' /p/tmp/pepflei/impact_data_explorer_data/ tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorere_api/data/
```


rsync -zarvm --include='*/' --exclude='*regAv*' /p/tmp/pepflei/impact_data_explorer_data/cou_data tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer/data/


rsync -zarvm /p/tmp/pepflei/impact_data_explorer_data/wlvl_diff tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer/data/

rsync -zarvm /p/tmp/pepflei/impact_data_explorer_data/masks tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer/data/

rsync -zarvm /p/tmp/pepflei/impact_data_explorer_data/topojsons tooli@87-230-15-215.kundenadmin.hosteurope.de:/home/tooli/impact_data_explorer/data/

.

# tar

for iso in AUT BEL BGR HRV CYP CZE DNK EST FIN FRA DEU GRC HUN IRL ITA LVA LTU LUX MLT NLD POL PRT ROU SVK SVN ESP SWE; do tar -czf ${iso}.gz /p/tmp/pepflei/impact_data_explorer_data/cou_data/*/*${iso}*.nc; done;