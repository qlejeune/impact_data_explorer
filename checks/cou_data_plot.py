import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=['DEU'])
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['dis'])

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=False)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())

print(args)

wlvls = [str(w) for w in list(np.arange(1.0,5.0,0.1).round(1))]

for iso in args['isos']:
    print(iso)
    for var in args['vars']:
        print(var)

        sys.path.append('meta/variables/')
        exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

        nc = xr.load_dataset('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/' + '_'.join([iso,var])+'.nc')
        out_median = nc['median']
        upper_bound = nc['upper_bound']
        lower_bound = nc['lower_bound']

        for region in out_median.region.values:
            for aggr in out_median.aggregation.values:
                for season in out_median.season.values:
                    plt.close('all')
                    fig,ax = plt.subplots(nrows=1)
                    x = np.array(out_median.wlvl,np.float)

                    ax.set_title('_'.join([var,iso,region,aggr,season]))

                    ax.plot(x, out_median.loc[:,region,aggr,season])
                    ax.fill_between(x, out_median.loc[:,region,aggr,season]+lower_bound.loc[:,region,aggr,season], out_median.loc[:,region,aggr,season]+upper_bound.loc[:,region,aggr,season], alpha=0.5)
                    ax.set_ylabel(details['long_name']+' - '+details['rel-abs'])

                    if os.path.isdir('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou') == False:
                        os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou')
                    plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'+'_'.join([var,iso,region,aggr,season])+'.png', transparent=False,  bbox_inches='tight'); plt.close()






#
