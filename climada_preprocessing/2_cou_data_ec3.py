import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
isos = [info.attributes['adm0_a3'] for info in cous]

tmp = pd.read_table('meta/scenarios/nauels17gmd_MAGICC_GMT_RCP60.csv', sep=';', header=0)
rcp60 = pd.DataFrame()
rcp60['year'] = tmp.columns[1:]
rcp60['wlvl'] = np.array(tmp.iloc[2,1:].values, np.float).round(1)

wlvls = np.arange(1.0,4.3,0.1).round(1)

avail_years = np.arange(2020,2105,5)

wlvl_inter = {}
for wlvl in wlvls:
    year = int(rcp60.loc[rcp60.wlvl == wlvl, 'year'].iloc[0])
    if year in avail_years:
        wlvl_inter[wlvl] = {'years':[year], 'weight':[1]}
    else:
        year_deviations = np.abs(avail_years - year)
        used_id = np.argsort(year_deviations)[:2]
        if np.max(year_deviations[used_id]) < 5:
            years = avail_years[used_id]
            deviations = year_deviations[used_id]
            wlvl_inter[wlvl] = {'years':years, 'weight':(1 / deviations) / (1 / deviations).sum() }

all_data = xr.DataArray(coords={'var':['ec3','ec4'], 'iso':isos, 'wlvl':[str(w) for w in wlvls]}, dims=['var','iso','region','wlvl','aggregation','season'])

for iso in isos:
    print(iso)
    file_ = '/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/TC/csv/TropCyclone_Impact_'+iso+'.csv'
    if os.path.isfile(file_):
        raw = pd.read_table(file_, sep=',')
        if 'rp100rcp6.0' in raw.columns:
            raw = raw[['years','aai_agg_rcp6.0','rp100rcp6.0']]

            out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])
            dummy_bound = xr.DataArray(0., coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])
            for wlvl,details in wlvl_inter.items():
                tmp = raw.loc[np.isin(raw.years,details['years']), 'aai_agg_rcp6.0'].values
                tmp = np.sum(tmp * details['weight'])
                out_median.loc[iso,str(wlvl),'other','annual'] = tmp

            all_data.loc['ec3',iso] = out_median
            # print(out_median)
            out_median = (out_median - out_median.sel(wlvl='1.1')) / out_median.sel(wlvl='1.1') * 100
            xr.Dataset({'median':out_median, 'lower_bound':dummy_bound, 'upper_bound':dummy_bound}).to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/cou_data/ec3/'+'_'.join([iso,'ec3'])+'.nc')

            out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])
            for wlvl,details in wlvl_inter.items():
                tmp = raw.loc[np.isin(raw.years,details['years']), 'rp100rcp6.0'].values
                tmp = np.sum(tmp * details['weight'])
                out_median.loc[iso,str(wlvl),'other','annual'] = tmp

            all_data.loc['ec4',iso] = out_median
            # print(out_median)
            out_median = (out_median - out_median.sel(wlvl='1.1')) / out_median.sel(wlvl='1.1') * 100
            xr.Dataset({'median':out_median, 'lower_bound':dummy_bound, 'upper_bound':dummy_bound}).to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/cou_data/ec4/'+'_'.join([iso,'ec4'])+'.nc')

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
continents = np.unique([info.attributes['continent'] for info in cous])

continent_dict = {'GLOBAL':all_data.iso.values}
for continent in continents:
    print(continent)
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = np.unique([info.attributes['adm0_a3'] for info in cous if info.attributes['continent']==continent])
    if continent == 'Europe':
        isos = [iso for iso in isos if iso != 'RUS']

    continent_dict[continent] = isos


for continent,isos in continent_dict.items():
    continent = continent.upper().replace(' ','_')
    for var in ['ec3','ec4']:
        out_median = xr.DataArray(coords={'region':[continent], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, \
                                  dims=['region','wlvl','aggregation','season'])
        tmp = all_data.loc[var,isos].sum('iso').squeeze()
        out_median.loc[continent,:,'other','annual'] = tmp
        out_median = (out_median - out_median.sel(wlvl='1.1')) / out_median.sel(wlvl='1.1') * 100
        xr.Dataset({'median':out_median, 'lower_bound':out_median.copy()*0, 'upper_bound':out_median.copy()*0}).to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'+'_'.join([continent.upper(),var])+'.nc')

#
