
import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import xesmf as xe

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

tmp = pd.read_table('meta/scenarios/nauels17gmd_MAGICC_GMT_RCP60.csv', sep=';', header=0)
rcp60 = pd.DataFrame()
rcp60['year'] = tmp.columns[1:]
rcp60['wlvl'] = np.array(tmp.iloc[2,1:].values, np.float).round(1)

wlvls = [1.1] + list(np.arange(1.0,4.0,0.5).round(1))

avail_years = np.arange(2020,2105,5)
wlvl_inter = {}
for wlvl in wlvls:
    year = int(rcp60.loc[rcp60.wlvl == wlvl, 'year'].iloc[0])
    if year in avail_years:
        wlvl_inter[wlvl] = {'years':[year], 'weight':[1]}
    else:
        year_deviations = np.abs(avail_years - year)
        used_id = np.argsort(year_deviations)[:2]
        if np.max(year_deviations[used_id]) < 5:
            years = avail_years[used_id]
            deviations = year_deviations[used_id]
            wlvl_inter[wlvl] = {'years':years, 'weight':(1 / deviations) / (1 / deviations).sum() }

os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/raw/ec3/%s/%s/%s' %('gcmD','rcp60','climada'))
for wlvl,details in wlvl_inter.items():
    if len(details['years']) == 1:
        tmp = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year%s_rcp60.nc' %(details['years'][0]))['Band1']
        xr.Dataset({'ec3':tmp}).to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/raw/ec3/%s/%s/%s/%s_%s_%s_ec3_%s.nc' %('gcmD','rcp60','climada','gcmD','rcp60','climada',wlvl))
    else:
        tmp1 = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year%s_rcp60.nc' %(details['years'][0]))['Band1']
        tmp2 = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year%s_rcp60.nc' %(details['years'][1]))['Band1']
        tmp = details['weight'][0] * tmp1 + details['weight'][1] * tmp2
        xr.Dataset({'ec3':tmp}).to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/raw/ec3/%s/%s/%s/%s_%s_%s_ec3_%s.nc' %('gcmD','rcp60','climada','gcmD','rcp60','climada',wlvl))





