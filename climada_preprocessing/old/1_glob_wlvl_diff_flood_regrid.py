import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import xesmf as xe

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

wlvl_dict = {i+1:str(w) for i,w in enumerate(np.arange(1.5,3.5,0.5).round(1))}
wlvl_dict[0] = '1.1'

var = 'ec2'
path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
os.system('mkdir -p '+path_out)

# regrid to 720x360
ref_grid = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/tasAdjust/tasAdjust_1.5_1.4_annual.nc')['var']
path_out_regrid = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'_720x360/'
os.system('mkdir -p '+path_out_regrid)

for fl in glob.glob(path_out+'*'):

    data = xr.open_dataset(fl)['var']
    model_agreement = xr.open_dataset(fl)['model_agreement']

    regridder = xe.Regridder(data, ref_grid, 'bilinear', reuse_weights=True)
    data_regrid = regridder(data)

    outPath = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'_720x360/'
    if os.path.isdir(outPath) == False:
        os.system('mkdir -p '+outPath)
    xr.Dataset({'var':regridder(data), 'model_agreement':regridder(model_agreement)}).to_netcdf(fl.replace(var,var+'_720x360'))
