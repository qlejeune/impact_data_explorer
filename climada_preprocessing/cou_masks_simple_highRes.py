import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json

import cartopy.io.shapereader as shapereader


sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)


import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=[])

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=False)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())

print(args)

if len(args['isos']) == 0:
	cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
	isos = [info.attributes['adm0_a3'] for info in cous]
else:
    isos = args['isos']

large = []
small = []
for iso in isos:
    print(iso)
    if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/region_names.pkl'):
        if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/TC/csv/TropCyclone_Impact_'+iso+'.csv'):
            data = pd.read_table('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/TC/csv/TropCyclone_Impact_'+iso+'.csv', sep=',')
            if np.sum(data['aai_agg_rcp6.0']) != 0:
                if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*simplify0.05*')) == 0 or args['overwrite']:

                    try:
                        COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
                        COU.load_shapefile()

                        bounds = COU._region_polygons[iso].bounds
                        rectangle_area = (bounds[2] - bounds[0]) * (bounds[3] - bounds[1])
                        
                        print('rectangle area',rectangle_area)
                        if rectangle_area > 1000:
                            COU.create_masks_overlap(input_file='/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year2020_rcp26_720x360.nc', simplify_polygons=0.05, region_names=[iso])

                        COU.create_masks_overlap(input_file='/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year2020_rcp26.nc', simplify_polygons=0.05, region_names=[iso])
                    except:
                        print('some issue with ',iso)







#