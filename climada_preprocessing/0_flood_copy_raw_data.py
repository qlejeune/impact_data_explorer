import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import xesmf as xe

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

wlvl_dict = {i+1:str(w) for i,w in enumerate(np.arange(1.5,3.5,0.5).round(1))}
wlvl_dict[0] = '1.1'

var = 'ec2'
path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
os.system('mkdir -p '+path_out)

for proj_wlvl_id in sorted(wlvl_dict.keys()):
    proj_wlvl = wlvl_dict[proj_wlvl_id]
    # here I load projection slices
    files_all = glob.glob('/p/projects/isimip/isimip/inga/ngfs/global_impact/warming_level_'+proj_wlvl+'/*')
    files = [f for f in files_all if proj_wlvl in f]
    # files = sorted(files)[:10]
    IDs = [f.split('/')[-1].replace('_'+proj_wlvl+'.nc','').replace('global_impact_ZWE_','') for f in files]
    proj = xr.open_mfdataset(files, concat_dim='ID')['Band1']
    proj = proj.assign_coords(ID=IDs)


    for ref_wlvl_id in range(proj_wlvl_id):
        ref_wlvl = wlvl_dict[ref_wlvl_id]
        print(proj_wlvl, ref_wlvl)

        # here I load all files for the warming level
        files_all = glob.glob('/p/projects/isimip/isimip/inga/ngfs/global_impact/warming_level_'+ref_wlvl+'/*')
        files = [f for f in files_all if ref_wlvl in f]
        # files = sorted(files)[:10]
        IDs = [f.split('/')[-1].replace('_'+ref_wlvl+'.nc','').replace('global_impact_ZWE_','') for f in files]
        ref = xr.open_mfdataset(files, concat_dim='ID')['Band1']
        ref = ref.assign_coords(ID=IDs)

        # here I make a selection of IDs that are available in ref and proj
        IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]


        # here for annual data
        out_file = path_out+'_'.join([var,proj_wlvl,ref_wlvl,'annual'])+'.nc'
        if os.path.isfile(out_file) == False or args['overwrite']:
            tmp_proj,tmp_ref = proj,ref

            # this is now the change in the the var for a warming level for all different gcm-impModel-rcp combinations
            diff_all = (tmp_proj.sel(ID=IDs) - tmp_ref.sel(ID=IDs))

            # prepare to average over rcps
            # IDs_gcm_im -> same length as IDs but with rcp information omitted
            # IDs_gcm_im_unique -> removed duplicates in IDs_gcm_im
            IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in IDs])
            IDs_gcm_im_unique = np.unique(IDs_gcm_im)

            # average over rcps
            gcm_im_combis = []
            for id_ in IDs_gcm_im_unique:
                gcm_im_combis.append(diff_all[IDs_gcm_im == id_].mean('ID'))
            diff = xr.concat(gcm_im_combis, dim='ID')
            diff = diff.assign_coords(ID=IDs_gcm_im_unique)

            # remove infs
            diff.values[np.isfinite(diff.values) == False] = np.nan

            # get the ensemble mean
            ens_mean = diff.mean('ID')

            # model agreement
            model_agreement = ens_mean.copy() * 0.0
            for ID in diff.ID.values:
                model_agreement += np.sign(diff.loc[ID]) == np.sign(ens_mean)
            model_agreement /= float(len(diff.ID.values))

            ds = xr.Dataset({'var':ens_mean, 'model_agreement':model_agreement})
            ds.attrs = {'GCM-IM input':diff.ID.values, 'GCM-IM-RCP available':list(IDs)}
            ds.to_netcdf(out_file)
            gc.collect()
