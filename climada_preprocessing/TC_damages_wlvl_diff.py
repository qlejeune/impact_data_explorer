import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import cartopy.io.shapereader as shapereader
import xesmf as xe

wlvl_dict = {
    '1.5' : 2038,
    '2.0' : 2059,
    '2.5' : 2078,
    '3.0' : 2087,
    '3.5' : 2095,
}

wlvl_dict = {
    '1.5' : 2035,
    '2.0' : 2060,
    '2.5' : 2080,
    '3.0' : 2085,
    '3.5' : 2095,
}

var = 'ec3'


path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
os.system('mkdir -p '+path_out)

# regrid to 720x360
ref_grid = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/tasAdjust/tasAdjust_1.5_1.4_annual.nc')['var']
path_out_regrid = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'_720x360/'
os.system('mkdir -p '+path_out_regrid)

ref = xr.load_dataset('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year2020_rcp60.nc')['Band1']

for wlvl, year in wlvl_dict.items():
    proj = xr.load_dataset('/p/tmp/pepflei/impact_data_explorer_data/CLIMADA/raw/ImpWorld_year'+str(year)+'_rcp60.nc')['Band1']

    diff = proj - ref

    ds = xr.Dataset({'var':diff, 'model_agreement':diff*0+1}).to_netcdf(path_out+'_'.join([var,wlvl,'1.1','annual'])+'.nc')

    regridder = xe.Regridder(proj, ref_grid, 'bilinear', reuse_weights=True)
    proj_regrid = regridder(proj)
    ref_regrid = regridder(ref)

    diff_regrid = proj_regrid - ref_regrid

    ds = xr.Dataset({'var':diff_regrid, 'model_agreement':diff_regrid*0+1}).to_netcdf(path_out_regrid+'_'.join([var+'_720x360',wlvl,'1.1','annual'])+'.nc')






#
